<?php

namespace Drupal\commerce_sheets_product\Plugin\CommerceSheets\EntityFormat;

use Drupal\commerce_sheets\EntityFormat\ContentEntityFormatBase;

/**
 * Provides a format plugin for attribute values.
 *
 * @CommerceSheetsEntityFormat(
 *   id = "attribute_value",
 *   label = @Translation("Attribute value")
 * )
 */
class AttributeValue extends ContentEntityFormatBase {}
