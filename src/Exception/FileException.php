<?php

namespace Drupal\commerce_sheets\Exception;

/**
 * Exception for errors related to import/export files.
 */
class FileException extends \Exception {}
