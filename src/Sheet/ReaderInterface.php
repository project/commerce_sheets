<?php

namespace Drupal\commerce_sheets\Sheet;

use Drupal\commerce_sheets\Entity\ImportInterface;

/**
 * Defines the interface for all Commerce Sheets reader services.
 *
 * The reader service is responsible for converting the data in the file
 * associated with the given Import entity and updating the corresponding
 * entitiy(ies).
 */
interface ReaderInterface {

  /**
   * Reads the given Import and updates the corresponding entities.
   *
   * @param \Drupal\commerce_sheets\Entity\ImportInterface $import
   *   The Import entity to read.
   * @param array $options
   *   An array of options that configure the behavior of the reader.
   *   Currently supported options are:
   *   - limit (int, defaults to `NULL`): The maximum number of rows to read.
   *   - offset (int, defaults to `0`): The number of rows to skip at the start.
   *
   * @return array
   *   A numerical array containing the following items:
   *   - (bool): `TRUE` if all available rows where read including the last row
   *     in the sheet, `FALSE` otherwise i.e. if  reading reached the limit
   *     before reaching the last row.
   *   - (int): The number of rows that were read.
   */
  public function read(ImportInterface $import, array $options = []);

  /**
   * Reads and returns the format plugin definition for the given Import.
   *
   * @param \Drupal\commerce_sheets\Entity\ImportInterface $import
   *   The Import entity to read the format plugin definition for.
   *
   * @return array
   *   An associative array with the following elements:
   *   plugin_id: The plugin ID.
   *   plugin_configuration: The plugin configuration.
   */
  public function getFormatDefinition(ImportInterface $import);

}
