<?php

namespace Drupal\commerce_sheets\EntitySync\Runner;

// Drupal modules.
use Drupal\entity_sync\Entity\Runner\RunnerBase;
use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\entity_sync\StateManagerInterface;
// Drupal core.
use Drupal\Core\Entity\EntityTypeManagerInterface;
// Third-party libraries.
use Psr\Log\LoggerInterface;

/**
 * Runner for operation types that imports lists of entities.
 */
class EntityImportList extends RunnerBase {

  /**
   * The Entity Synchronization entity import manager.
   *
   * @var \Drupal\entity_sync\Import\ManagerInterface
   */
  protected $importManager;

  /**
   * Constructs a new EntityImportListRunner object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\entity_sync\Import\ManagerInterface $import_manager
   *   The Entity Synchronization entity import manager.
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Synchronization state manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    ImportManagerInterface $import_manager,
    StateManagerInterface $state_manager
  ) {
    parent::__construct($entity_type_manager, $logger);

    $this->importManager = $import_manager;
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Supported context options are:
   * - filters (array, optional) An array containing the filters to pass to the
   *   import manager.
   * - options (array) An associative array with options to pass to the import
   *   manager.
   *
   * @see \Drupal\entity_sync\Import\ManagerInterface::importRemoteList()
   */
  protected function doRun(
    OperationInterface $operation,
    array $context
  ) {
    $storage = $this->entityTypeManager
      ->getStorage('commerce_sheets_import');

    // Move the import to `running` state.
    $import = $context['options']['client']['import_entity'];
    $state_item = $import->get('state')->first();
    $state_item->applyTransitionById('run');
    $storage->save($import);

    // Run the import, and:
    // - If completed, moved to `complete` state.
    // - If any error occurred, moved to `failed` state.
    // - If the run was successful but there are more entities to import on the
    //   next run, move back to the `scheduled` state so that it will be picked
    //   up next time.
    $transition_id = 'complete';
    try {
      $this->importManager->importRemoteList(
        $operation->bundle(),
        $context['filters'] ?? [],
        $context['options'] ?? []
      );

      $next_offset = $this->stateManager->getLastRunItem(
        $operation->bundle(),
        'import_list',
        'next_offset'
      );
      if ($next_offset) {
        $transition_id = 'schedule';
      }
    }
    catch (\Throwable $throwable) {
      $transition_id = 'fail';
      throw $throwable;
    }
    finally {
      $state_item->applyTransitionById($transition_id);
      $storage->save($import);
    }
  }

}
