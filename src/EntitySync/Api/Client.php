<?php

namespace Drupal\commerce_sheets\EntitySync\Api;

// Drupal modules.
use Drupal\commerce_sheets\Sheet\ReaderInterface;
use Drupal\entity_sync\Client\ClientInterface;
// Third-party libraries.
use KrystalCode\ApiIterator\ClientInterface as IteratorClientInterface;
use KrystalCode\ApiIterator\Iterator as ApiIterator;

/**
 * The API client Entity Sync adapter for the sheet reader/writer.
 */
class Client implements ClientInterface, IteratorClientInterface {

  /**
   * Constructs a new Client object.
   *
   * @param \Drupal\commerce_sheets\Sheet\ReaderInterface $reader
   *   The Commerce Sheets reader.
   */
  public function __construct(
    ReaderInterface $reader
  ) {
    $this->reader = $reader;
  }

  /**
   * {@inheritdoc}
   *
   * Supported options are:
   * - import_entity (\Drupal\commerce_sheets\Entity\ImportInterface, required):
   *   The import that will be executed.
   * - page (int, optional, defaults to `1`): The page to start importing at.
   * - limit (int, optional, defaults to `100`): The number of entities to
   *   import per page.
   * - delay (array, optional, defaults to `NULL`): The delay to wait after
   *   importing a page and before proceeding to the next one. This is commonly
   *   not needed for spreadsheet imports that are read from local files, but it
   *   might be useful for spreading out the processing power. For the supported
   *   format see `\KrystalCode\ApiIterator::$delay`.
   *
   * @I Support limit/delay client options in sync configuration
   */
  public function importList(array $filters = [], array $options = []) {
    return $this->list(
      $this->buildListRequestOptions($filters, $options),
      [
        'import_entity' => $options['import_entity'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function importEntity($id) {
    throw new \Exception('Importing individual entities using Commerce Sheets is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities using Commerce Sheets is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function update($id, array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities using Commerce Sheets is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function list(array $options = [], array $query = []) {
    $options = array_merge(
      [
        'page' => 1,
        'limit' => 100,
        'delay' => NULL,
      ],
      $options
    );
    $query = array_merge(
      [],
      $query
    );
    $query['offset'] = ($options['page'] - 1) * $options['limit'];
    $query['limit'] = $options['limit'];

    // By default, we return an iterator. Caller code can then loop over it
    // and the iterator will internally handle reading the objects for each
    // page.
    if (empty($options['bypass_iterator'])) {
      return new ApiIterator(
        $this,
        $options['page'],
        $options['limit'],
        $query,
        FALSE,
        $options['delay']
      );
    }

    // Otherwise, either it is requested to fetch the results directly, or
    // this is a call by the iterator while being looped over. Read the entities
    // and return the results.
    unset($options['bypass_iterator']);

    if (!isset($query['import_entity'])) {
      throw new \RuntimeException(
        'No Commerce Sheets import entity provided to the client.'
      );
    }

    [$finished, $count] = $this->reader->read(
      $query['import_entity'],
      [
        'limit' => $query['limit'],
        'offset' => $query['offset'],
      ]
    );

    // The plan for Commerce Sheets is to be eventually be rewritten from the
    // ground up on top of Entity Synchronization so that we natively take
    // advantage of all the features that it provides. However, right now it is
    // not so straight-forward to have the reader to simply read the entries and
    // return them so that they are processed by Entity Synchronization.
    // Processing still needs to be done by the reader until entity formatting
    // and field handler plugins are ported to make use of Entity
    // Synchronization metchanisms.
    // So, to still take advantage of the batching provided by Entity
    // Synchronization, we create stub entities just so that import manager's
    // iterator can do its job.
    $objects = [];
    for ($i = 0; $i < $count; $i++) {
      $objects[] = new \stdClass();
    }

    return [
      new \CachingIterator(
        new \ArrayIterator($objects),
        \CachingIterator::FULL_CACHE
      ),
      !$finished,
      $query,
    ];
  }

  /**
   * Builds and returns the options for the API iterator client.
   *
   * @param array $filters
   *   The filters passed to the client.
   * @param array $options
   *   The options passed to the client.
   *
   * @return array
   *   The options array to be passed to the list request.
   */
  protected function buildListRequestOptions(
    array $filters,
    array $options
  ) {
    // Support delay between requests; it is not particularly meaningful in most
    // cases for files stored locally, but there could be cases where we might
    // want to limit the rate of I/O operations or of operating on an external
    // filesystem. If not given, no delay will be applied.
    $request_options = [
      'delay' => $options['delay'] ?? NULL,
    ];

    // Number of items per page. If not given, the `list()` method will apply
    // the default of 100.
    if (isset($options['limit'])) {
      $request_options['limit'] = $options['limit'];
    }
    // Page to start at; we may be continuing a state-managed import that did
    // not finish all pages in its last run.
    // If not given, the `list()` method will apply the default of 1 i.e. start
    // at the first page.
    if (!empty($filters['offset']) && isset($request_options['limit'])) {
      $request_options['page'] = ((int) floor($filters['offset'] / $request_options['limit'])) + 1;
    }

    return $request_options;
  }

}
