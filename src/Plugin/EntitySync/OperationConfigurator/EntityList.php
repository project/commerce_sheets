<?php

namespace Drupal\commerce_sheets\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\DefaultPluginBase;
use Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait;

/**
 * Plugin for operations that relate to a list of entities.
 *
 * That is, for operations importing or exporting entities to files. Currently
 * only imports are supported. Support for exports should be added in this
 * plugin.
 *
 * Right now, only one operation type is required to be created with this
 * plugin; that operation type will handle all imports since all entity formats
 * for all imports will still be handled internally by the current mechanisms of
 * Commerce Sheets. This might change when the module is fully rewritten on top
 * top of Entity Synchronization in a future major version.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "commerce_sheets_entity_list",
 *   label = @Translation("Commerce Sheets entity list import"),
 *   description = @Translation(
 *     "Use 'Commerce Sheets entity list import' types to import a list of rows from a spreadsheet into Drupal entities."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class EntityList extends DefaultPluginBase {

  use EntityTrait;

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'commerce_sheets.entity_sync_runner.entity_import_list';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'client' => [
          'type' => 'service',
          'service' => 'commerce_sheets.entity_sync_client.default',
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();

    // Add the dynamic entity reference field that will be referencing the
    // Import entity.
    $fields += $this->buildEntityBundleFieldDefinitions();

    return $fields;
  }

}
