<?php

namespace Drupal\commerce_sheets\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Event\PreInitiateOperationEvent;
// Third-party libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Prevents Entity Synchronization from importing individual entities.
 *
 * Until the Commerce Sheets module is rewritten from the ground up on top of
 * the Entity Synchronization, we actually do the imports in the Reader provided
 * by this module. There is therefore no need for the Entity Synchronization's
 * import manager to actually run the import action for each entity while
 * looping over the results returned by the client adapter.
 */
class CancelRemoteEntityImport implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::REMOTE_ENTITY_PRE_INITIATE => ['cancelOperation', 0],
    ];
    return $events;
  }

  /**
   * Prevents Entity Synchronization from importing individual entities.
   *
   * @param \Drupal\entity_sync\Event\PreInitiateOperationEvent $event
   *   The pre-initiate operation event.
   */
  public function cancelOperation(PreInitiateOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    // Expected behavior always, no need to log an message.
    $event->cancel();
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to Commerce Sheets import operations managed by Entity
   * Synchronization.
   *
   * @param \Drupal\entity_sync\Event\PreInitiateOperationEvent $event
   *   The pre-initiate event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(PreInitiateOperationEvent $event) {
    $sync = $event->getSync();
    if (!$sync instanceof OperationTypeInterface) {
      return FALSE;
    }

    if ($sync->getPluginId() !== 'commerce_sheets_entity_list') {
      return FALSE;
    }

    $context = $event->getContext();
    // Only proceed if the context indicates that the import is managed.
    if (($context['state']['manager'] ?? NULL) !== 'entity_sync') {
      return FALSE;
    }

    return TRUE;
  }

}
