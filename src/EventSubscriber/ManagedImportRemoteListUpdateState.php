<?php

namespace Drupal\commerce_sheets\EventSubscriber;

// This module.
use Drupal\commerce_sheets\Entity\ImportInterface;
// Drupal modules.
use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\ListFiltersEvent;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\StateManagerInterface;
// Third-party libraries.
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Updates the state with data required for paged imports.
 */
class ManagedImportRemoteListUpdateState implements EventSubscriberInterface {

  /**
   * The Entity Sync state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new ManagedImportRemoteListFilters object.
   *
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Sync state manager service.
   */
  public function __construct(StateManagerInterface $state_manager) {
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // This needs to run after the subscriber provided by the Entity
      // Synchronization module so that the values set here are not overridden
      // or removed over there.
      Events::REMOTE_LIST_FILTERS => ['updateState', -10],
    ];
    return $events;
  }

  /**
   * Updates the state with data required for paged imports.
   *
   * For supporting paged imports, on top of the values set by Entity
   * Synchronization we need to:
   * - Set the ID of the import so that it can be continued in the next run.
   * - Unset the offset if the last run was for a different import.
   *
   * @param \Drupal\entity_sync\Import\Event\ListFiltersEvent $event
   *   The list filters event.
   */
  public function updateState(ListFiltersEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    $import = $event
      ->getContext()['operation_entity']
      ->get(OperationField::ENTITY)
      ->entity;
    if (!$import || !$import instanceof ImportInterface) {
      throw new \RuntimeException(sprintf(
        'The import entity for the operation with ID "%s" was not found.',
        $event->getContext()['operation_entity']->id()
      ));
    }

    $sync_id = $event->getSync()->id();
    $current_run = $this->stateManager->getCurrentRun($sync_id, 'import_list');

    // Update the offset, if necessary - see `$this::updateOffset()`.
    $this->updateOffset($import->id(), $current_run, $sync_id);

    $data = $current_run['data'] ?? [];
    $data['import_id'] = $import->id();

    $this->stateManager->setCurrentRun(
      $sync_id,
      'import_list',
      $current_run['run_time'],
      $current_run['start_time'] ?? NULL,
      $current_run['end_time'] ?? NULL,
      $current_run['limit'] ?? NULL,
      $current_run['offset'] ?? NULL,
      $data
    );
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to Commerce Sheets import operations managed by Entity
   * Synchronization.
   *
   * @param \Drupal\entity_sync\Import\Event\ListFiltersEvent $event
   *   The list filters event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(Event $event) {
    $sync = $event->getSync();
    if (!$sync instanceof OperationTypeInterface) {
      return FALSE;
    }

    if ($sync->getPluginId() !== 'commerce_sheets_entity_list') {
      return FALSE;
    }

    $context = $event->getContext();
    // Only proceed if the context indicates that the import is managed.
    if (($context['state']['manager'] ?? NULL) !== 'entity_sync') {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Updates the offset to make sure we don't carry offset from another import.
   *
   * The next offset is cleaned when the import successfully completes i.e.
   * when there are no more pages to process. However, if the import fails the
   * next offset is not cleaned up and the state will still be indicating a
   * next offset for the current import to continue from. We therefore need to
   * clean it up here and make sure that we don't carry over the next offset
   * from a different import.
   *
   * We do not do this in a terminate subscriber or when the runner terminates
   * the operation in the last run for two reasons:
   * - To still have it temporarily available in case it is needed for
   *   troubleshooting the failure of the last run - even though the value of
   *   this is limited since it will be replaced by the details of the current
   *   run when it finishes.
   * - More importantly, a fatal failure that causes abrupt program execution
   *   might prevent the program to reach the point of removing the next offset.
   *   In that case we would still want to have the logic here as well.
   *
   * At this point the offset will have already been carried over from the last
   * run to this run by the event subscriber provided by Entity Synchronization.
   * We therefore simply need to unset it if the last run executed a different
   * import than this one.
   *
   * @param int|string $import_id
   *   The ID of the import currently being run.
   * @param array $current_run
   *   The state of the current run.
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type entity that defines the
   *   operation currently being run.
   */
  protected function updateOffset(
    $import_id,
    array &$current_run,
    string $sync_id
  ) {
    $last_run = $this->stateManager->getLastRun($sync_id, 'import_list');

    // If we don't have an offset carried over to the current run, nothing to
    // do.
    if (($current_run['offset'] ?? NULL) === NULL) {
      return;
    }
    // Otherwise, if we do and the last run executed the same import that we are
    // currently executing, we must be continuing where it stopped. Nothing to
    // do, we do want to keep the offset.
    if (($last_run['data']['import_id'] ?? NULL) == $import_id) {
      return;
    }

    // Otherwise, the offset must be coming from a different import that failed.
    // Unset it.
    $current_run['offset'] = NULL;
  }

}
